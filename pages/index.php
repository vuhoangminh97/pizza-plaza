<?php

// pages/index.php

$currentSite = isset($_GET['site']) ? $_GET['site'] : 'main';

// Define breadcrumbs
$breadcrumbs = [
    'main' => 'Startseite',
    'about' => '&Uuml;ber uns',
    'contact' => 'Kontakt',
    'imprint' => 'Impressum',
    'order' => 'Online bestellen',
    'order_success' => 'Bestellbestätigung',
    'checkout' => 'Checkout'
];

// Define the title for the breadcrumb based on the current site
$breadcrumbTitle = isset($breadcrumbs[$currentSite]) ? $breadcrumbs[$currentSite] : 'Unbekannt';

?>

<!-- Breadcrumb navigation -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="?site=main">Pizza Plaza</a></li>
        <?php if ($currentSite !== 'main'): // If not the home page, show the breadcrumb ?>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $breadcrumbTitle; ?></li>
        <?php endif; ?>
    </ol>
</nav>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pizza Plaza</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>




<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="?site=main">Pizza Plaza</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if($currentSite === 'main') echo 'active'; ?>">
                <a class="nav-link" href="?site=main">Startseite</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'about') echo 'active'; ?>">
                <a class="nav-link" href="?site=about">&Uuml;ber uns</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'contact') echo 'active'; ?>">
                <a class="nav-link" href="?site=contact">Kontakt</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'imprint') echo 'active'; ?>">
                <a class="nav-link" href="?site=imprint">Impressum</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'order') echo 'active'; ?>">
                <a class="nav-link" href="?site=order">Online bestellen</a>
            </li>

        </ul>
    </div>

    <ul class="navbar-nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Language
            </a>
            <div class="dropdown-menu" aria-labelledby="languageDropdown">
                <a class="dropdown-item" href="?lang=en">English</a>
                <a class="dropdown-item" href="?lang=de">Deutsch</a>
            </div>
        </li>
    </ul>

    <div id="order-summary">
    <a href="?site=checkout">
        Items: <span id="item-count">0</span>,
        Total: $<span id="total-price">0.00</span>
    </a>
</div>
</nav>

<div class="container">
    <?php include $currentSite . '.php'; ?>
</div>

<div id="cookie-notice" style="display:none;">
    This website uses cookies to ensure you get the best experience.
    <button onclick="acceptCookies()">Accept</button>
</div>
<script>
    function updateOrderSummary() {
        var orderData = JSON.parse(localStorage.getItem('orderData'));
        if (orderData && orderData.pizzaItems) {
            var itemCount = orderData.pizzaItems.reduce((count, item) => count + parseInt(item.quantity), 0);
            var totalPrice = orderData.pizzaItems.reduce((total, item) => total + (parseFloat(item.price) * parseInt(item.quantity)), 0);

            document.getElementById('item-count').textContent = itemCount;
            document.getElementById('total-price').textContent = totalPrice.toFixed(2);
        } else {
            document.getElementById('item-count').textContent = '0';
            document.getElementById('total-price').textContent = '0.00';
        }
    }

    window.addEventListener('load', updateOrderSummary);
</script>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/popper.js/dist/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>

<?php
// Include your database connection file
require_once 'db.php';
require_once 'components/Article.php';

$article = new Article($db);
$pizzaOfTheDay = $article->getPizzaOfTheDay();
?>

<p class="lead">Herzlich Willkommen auf der Webseite von Pizza Plaza!</p>
<p>Wir sind ein fiktives Restaurant in Gießen/Langgöns. Auf unserer Webseite erfahren Sie nähere Informationen über uns, Wege, uns zu kontaktieren und erhalten bald die Möglichkeit, Bestellungen online durchzuführen.</p>
<p>Ihr Restaurant Pizza Plaza</p>

<!-- HTML to display the pizza of the day -->
<div id="pizza-of-the-day">
    <h2>Pizza of the Day</h2>
    <?php if ($pizzaOfTheDay): ?>
        <div class="pizza">
            <h3><?php echo htmlspecialchars($pizzaOfTheDay['name']); ?></h3>
            <p><?php echo htmlspecialchars($pizzaOfTheDay['description']); ?></p>
            <p>Today's Special Price: $<?php echo number_format($pizzaOfTheDay['price'], 2); ?></p>
        </div>
    <?php else: ?>
        <p>Check back tomorrow for a new delicious deal!</p>
    <?php endif; ?>
</div>



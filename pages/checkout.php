
<div id="checkout-container">
    <h1>Checkout</h1>
    <form action="/pages/process_order.php" method="post">
        <h2>Your Order</h2>
        <div id="order-items"></div>

        <div id="delivery-option">
            <input type="checkbox" id="delivery" name="delivery" onchange="toggleDeliveryFields()">
            <label for="delivery">Delivery (€1.50)</label>
            <label for="firstname">First Name:</label>
            <input type="text" id="firstname" name="firstname" required>
            <label for="lastname">Last Name:</label>
            <input type="text" id="lastname" name="lastname" required>
            <label for="phone">Phone Number:</label>
            <input type="tel" id="phone" name="phone" required>
        </div>

        <div id="address-fields">

            <label for="street">Street:</label>
            <input type="text" id="street" name="street">
            <label for="streetnumber">Street Number:</label>
            <input type="text" id="streetnumber" name="streetnumber">
            <label for="zip">Zip Code:</label>
            <input type="text" id="zip" name="zip">
            <label for="city">City:</label>
            <input type="text" id="city" name="city">

        </div>
        <input type="hidden" id="order-data-input" name="orderData">
        <input type="submit" value="Complete Order">
    </form>
</div>

<script>
    function loadCartItems() {

        var orderData = JSON.parse(localStorage.getItem('orderData'));
        var orderItemsDiv = document.getElementById('order-items');

        if (orderData && orderData.pizzaItems) {
            document.getElementById('order-data-input').value = JSON.stringify(orderData);

            orderData.pizzaItems.forEach(function(item) {
                var itemDiv = document.createElement('div');
                var extras = item.extras.length > 0 ? item.extras.join(', ') : 'No Extras';
                var totalPrice = (parseFloat(item.price) * parseInt(item.quantity)).toFixed(2);
                itemDiv.innerHTML = `<strong>${item.name}</strong> - Quantity: ${item.quantity} - Price: €${totalPrice} - Extras: ${extras}`;
                orderItemsDiv.appendChild(itemDiv);
            });
        } else {
            orderItemsDiv.innerHTML = '<p>No items in the cart.</p>';
        }
    }
    function calculateOrderTotal() {
        var orderData = JSON.parse(localStorage.getItem('orderData'));
        return orderData?.pizzaItems?.reduce(function(total, item) {
            return total + (item.price * item.quantity);
        }, 0);
    }

    function updateDeliveryOptions() {
        var orderTotal = calculateOrderTotal();
        var deliveryOption = document.getElementById('delivery-option');
        var addressFields = document.getElementById('address-fields');

        if (orderTotal >= 10) {
            deliveryOption.style.display = 'block';
            if (orderTotal >= 25) {
                // Waive delivery fee
            } else {
                // Apply €1.50 delivery fee
            }
        } else {
            deliveryOption.style.display = 'none';
        }
    }

    function toggleDeliveryFields() {
        var deliveryCheckbox = document.getElementById('delivery');
        var addressFields = document.getElementById('address-fields');
        if (deliveryCheckbox.checked) {
            addressFields.style.display = 'block';
        } else {
            addressFields.style.display = 'none';
        }
    }

    window.addEventListener('load', function() {
        loadCartItems();
        updateDeliveryOptions();
    });

</script>
<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f4f4f4;
        margin: 0;
        padding: 20px;
    }
    #checkout-container {
        background: white;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
    h1, h2 {
        color: #333;
    }
    label {
        display: block;
        margin: 10px 0 5px;
    }
    input[type="text"],
    input[type="tel"],
    input[type="submit"] {
        width: 100%;
        padding: 10px;
        margin-bottom: 10px;
        border: 1px solid #ddd;
        border-radius: 4px;
        box-sizing: border-box;
    }
    input[type="submit"] {
        background-color: #5cb85c;
        color: white;
        border: none;
        cursor: pointer;
    }
    input[type="submit"]:hover {
        background-color: #4cae4c;
    }
    #delivery-option {
        margin: 20px 0;
        padding: 10px;
        background-color: #f9f9f9;
        border: 1px solid #e9e9e9;
        border-radius: 4px;
    }
    #address-fields {
        background-color: #e9e9e9;
        padding: 15px;
        border-radius: 4px;
        margin-bottom: 20px;
        display: none;
    }
</style>
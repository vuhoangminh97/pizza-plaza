<?php
require_once 'db.php';
require_once 'components/Order.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    error_log("Received POST request with data: " . print_r($_POST, true));

    $orderData = json_decode($_POST['orderData'], true);
    error_log("Decoded orderData: " . print_r($orderData, true));
    
    $isDelivery = isset($_POST['delivery']) && $_POST['delivery'] == 'on';

    $customerInfo = [
        'firstname' => $_POST['firstname'] ?? 'Takeaway',
        'lastname' => $_POST['lastname'] ?? 'Takeaway',
        'street' => $isDelivery ? $_POST['street'] : 'Takeaway',
        'streetnumber' => $isDelivery ? $_POST['streetnumber'] : 'Takeaway',
        'zip' => $isDelivery ? $_POST['zip'] : 'Takeaway',
        'city' => $isDelivery ? $_POST['city'] : 'Takeaway',
        'phone' => $_POST['phone'] ?? 'Takeaway'
    ];

    error_log("Customer Info: " . print_r($customerInfo, true));

    $orderData['customerInfo'] = $customerInfo;

    $order = new Order($db);
    $result = $order->saveOrder($orderData);

    error_log("Result of saveOrder: " . print_r($result, true));

    if ($result['success']) {
        header('Location: /?site=order_success&orderId=' . $result['orderId']);
        exit;
    } else {
        echo "Order processing failed: " . $result['error'];
        error_log("Order processing failed: " . $result['error']);
    }
}
?>

<h1>Online bestellen</h1>

<form id="pizza-order-form" action="pages/process_order.php" method="post">

<?php
require_once 'db.php'; 
require_once 'components/Article.php';

// Fetch the pizzas with ingredients (extras)
$pizzas = (new Article($db))->getPizzasWithIngredients();

foreach ($pizzas as $pizza) {
    // Fetch the extras for this pizza
    $extras = (new Article($db))->getExtras();


    echo "<div class='pizza'>";
    echo "<h3>".htmlspecialchars($pizza['name'], ENT_QUOTES, 'UTF-8')." - \$".htmlspecialchars($pizza['price'], ENT_QUOTES, 'UTF-8')."</h3>";
    echo "<p>".htmlspecialchars($pizza['description'], ENT_QUOTES, 'UTF-8')."</p>"; // Display the description
    echo "<p>".htmlspecialchars($pizza['ingredients'], ENT_QUOTES, 'UTF-8')."</p>"; // Display the extras as ingredients
    
    // Display checkboxes for each extra
    foreach ($pizza['extras'] as $extra) {
        echo "<div class='extra'>";
        echo "<input type='checkbox' name='extras[".htmlspecialchars($pizza['ID'], ENT_QUOTES, 'UTF-8')."][".htmlspecialchars($extra['ID'], ENT_QUOTES, 'UTF-8')."]' value='".htmlspecialchars($extra['name'], ENT_QUOTES, 'UTF-8')."'>";
        echo "<label for='extras[".htmlspecialchars($pizza['ID'], ENT_QUOTES, 'UTF-8')."][".htmlspecialchars($extra['ID'], ENT_QUOTES, 'UTF-8')."]'>".htmlspecialchars($extra['name'], ENT_QUOTES, 'UTF-8')."</label>";
        echo "</div>";
    }
    
    echo "<input type='number' name='quantity[".htmlspecialchars($pizza['ID'], ENT_QUOTES, 'UTF-8')."]' min='1'>";
    echo "<button type='button' class='add-to-order' data-id='".htmlspecialchars($pizza['ID'], ENT_QUOTES, 'UTF-8')."' data-name='".htmlspecialchars($pizza['name'], ENT_QUOTES, 'UTF-8')."' data-price='".htmlspecialchars($pizza['price'], ENT_QUOTES, 'UTF-8')."'>Add to Order</button>";
    echo "</div>";
}
?>
    <input type="submit" value="Submit Order">
</form>

<script>
var orderItems = { pizzaItems: [] };

function addToOrder(pizzaId, pizzaName, pizzaPrice) {
    var quantityInput = document.querySelector('input[name="quantity['+pizzaId+']"]');
    var quantity = quantityInput.value;

    var extras = [];

    // Gather all selected extras for this pizza
    document.querySelectorAll('input[name^="extras['+pizzaId+']"]:checked').forEach(function(checkbox) {
        extras.push(checkbox.value);
    });

    orderItems.pizzaItems.push({
        id: pizzaId,
        name: pizzaName,
        price: pizzaPrice,
        quantity: quantity,
        extras: extras 
    });


    // Optionally, update the UI to reflect the item was added
    console.log("Added to order: ", pizzaName, orderItems);
}

document.querySelectorAll('.add-to-order').forEach(function(button) {
    button.addEventListener('click', function() {
        var pizzaId = this.getAttribute('data-id');
        var pizzaName = this.getAttribute('data-name');
        var pizzaPrice = this.getAttribute('data-price');
        addToOrder(pizzaId, pizzaName, pizzaPrice);
    });
});

document.getElementById('pizza-order-form').addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent the default form submission

    // Store orderItems in local storage
    localStorage.setItem('orderData', JSON.stringify(orderItems));

    // Optionally, redirect to another page, like a checkout page
    window.location.href = '/site?site=checkout';

    // If you still need to submit the form to the server, uncomment the following line
    // this.submit(); 
});
</script>
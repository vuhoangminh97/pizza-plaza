<?php
// pages/order_success.php

if (!isset($_GET['orderId'])) {
    // Redirect to the main page or show an error if no order ID is present
    header('Location: index.php');
    exit;
}

require_once 'db.php';
require_once 'components/Order.php';

$order = new Order($db);
$orderInfo = $order->getOrder($_GET['orderId']); // Fetch order details

if (!$orderInfo) {
    // Handle the case where the order is not found
    echo "Order not found.";
    exit;
}

// Assuming $orderInfo contains both customer info and pizza items
$customerInfo = $orderInfo['customerInfo'];
$pizzaItems = $orderInfo['pizzaItems'];

// Display order success information
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Success - Pizza Plaza</title>
    <!-- Include your stylesheets here -->
</head>
<body>
    <h1>Your Order is Confirmed!</h1>
    <p>Thank you for your order, <?php echo htmlspecialchars($customerInfo['firstname']); ?>!</p>
    
    <h2>Customer Information</h2>
    <p>Name: <?php echo htmlspecialchars($customerInfo['firstname'] . ' ' . $customerInfo['lastname']); ?></p>
    <p>Address: <?php echo htmlspecialchars($customerInfo['street'] . ' ' . $customerInfo['streetnumber'] . ', ' . $customerInfo['zip'] . ' ' . $customerInfo['city']); ?></p>
    <p>Phone: <?php echo htmlspecialchars($customerInfo['phone']); ?></p>

    <h2>Order Details</h2>
    <ul>
        <?php foreach ($pizzaItems as $item): ?>
            <li><?php echo htmlspecialchars($item['quantity'] . ' x ' . $item['name']); ?> - $<?php echo htmlspecialchars($item['price']); ?></li>
        <?php endforeach; ?>
    </ul>
    
    <a href="index.php">Return to Home</a>
</body>
</html>

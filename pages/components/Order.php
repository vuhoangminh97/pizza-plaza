<?php
// components/Order.php

class Order {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }
    public function getOrder($orderId) {
        try {
            // Query to fetch order details along with customer information
            $orderQuery = "
                SELECT o.ID as orderId, c.firstname, c.lastname, c.street, c.streetnumber, c.zip, c.city, c.phone, 
                       oi.quantity, p.name, p.price
                FROM `Order` o
                INNER JOIN Customer c ON o.Customer_ID = c.ID
                INNER JOIN OrderItems oi ON o.ID = oi.Order_ID
                INNER JOIN Pizzas p ON oi.Pizzas_ID = p.ID
                WHERE o.ID = ?";

            $stmt = $this->db->prepare($orderQuery);
            $stmt->execute([$orderId]);

            $orderData = [];
            while ($row = $stmt->fetch()) {
                // Assuming the first row contains all customer data and subsequent rows contain pizza items
                if (empty($orderData)) {
                    $orderData['customerInfo'] = [
                        'firstname' => $row['firstname'],
                        'lastname' => $row['lastname'],
                        'street' => $row['street'],
                        'streetnumber' => $row['streetnumber'],
                        'zip' => $row['zip'],
                        'city' => $row['city'],
                        'phone' => $row['phone']
                    ];
                }
                
                $orderData['pizzaItems'][] = [
                    'quantity' => $row['quantity'],
                    'name' => $row['name'],
                    'price' => $row['price']
                ];
            }

            return $orderData;

        } catch (Exception $e) {
            // Handle any exceptions (such as query failures)
            return ['error' => $e->getMessage()];
        }
    }

    public function saveOrder($orderData) {
        try {
            // Start transaction
            $this->db->beginTransaction();
            
            // Insert customer data and get the customer ID
            $customerStmt = $this->db->prepare("INSERT INTO Customer (firstname, lastname, street, streetnumber, zip, city, phone) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $customerStmt->execute([
                $orderData['customerInfo']['firstname'],
                $orderData['customerInfo']['lastname'],
                $orderData['customerInfo']['street'],
                $orderData['customerInfo']['streetnumber'],
                $orderData['customerInfo']['zip'],
                $orderData['customerInfo']['city'],
                $orderData['customerInfo']['phone']
            ]);
            $customerId = $this->db->lastInsertId();
            
            // Insert order data and get the order ID
            $orderStmt = $this->db->prepare("INSERT INTO `Order` (timestamp, Customer_ID) VALUES (NOW(), ?)");
            $orderStmt->execute([$customerId]);
            $orderId = $this->db->lastInsertId();
            
            // Insert order items
            $itemStmt = $this->db->prepare("INSERT INTO OrderItems (quantity, Order_ID, Pizzas_ID) VALUES (?, ?, ?)");
            foreach ($orderData['pizzaItems'] as $item) {
                $itemStmt->execute([
                    $item['quantity'],
                    $orderId,
                    $item['id']
                ]);
                // No need for lastInsertId if not using Extras
            }

            // Commit transaction
            $this->db->commit();
            
            return ['success' => true, 'orderId' => $orderId];
        } catch (Exception $e) {
            // Rollback transaction if something goes wrong
            $this->db->rollBack();
            
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }
}

?>
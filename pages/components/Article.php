<?php
// components/Article.php

class Article {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getPizzas() {
        $query = "SELECT * FROM pizzas";
        $stmt = $this->db->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getPizzasWithIngredients() {
        $query = "
            SELECT p.ID, p.name, p.price, p.description,
                   GROUP_CONCAT(e.name ORDER BY e.name ASC) as ingredients,
                   GROUP_CONCAT(e.ID ORDER BY e.name ASC) as extras_ids 
            FROM Pizzas p
            LEFT JOIN Pizza_has_Extra phe ON p.ID = phe.Pizzas_ID
            LEFT JOIN Extras e ON phe.Extras_ID = e.ID
            GROUP BY p.ID
        ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $pizzas = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // Process each pizza to include its specific extras
            foreach ($pizzas as $key => $pizza) {
                $extras_ids = explode(',', $pizza['extras_ids']);
                $pizzas[$key]['extras'] = $this->getSpecificExtras($extras_ids);
            }
    
            return $pizzas;
        } catch (PDOException $e) {
            // Handle exception here
            error_log($e->getMessage());
            return []; // Return an empty array as a fallback
        }
    }
    
    // Function to fetch specific extras by their IDs
    public function getSpecificExtras(array $extras_ids) {
        if (empty($extras_ids)) {
            return [];
        }
    
        $in_query = implode(',', array_fill(0, count($extras_ids), '?'));
        $query = "SELECT * FROM Extras WHERE ID IN ($in_query)";
        $stmt = $this->db->prepare($query);
    
        foreach ($extras_ids as $index => $id) {
            $stmt->bindValue(($index+1), $id);
        }
    
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    

    public function getExtras() {
        $query = "SELECT * FROM Extras";
        $stmt = $this->db->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getPizzaOfTheDay() {
        try {
            $dayOfYear = date('z');
            $countQuery = $this->db->query('SELECT COUNT(*) FROM Pizzas');
            if (!$countQuery) {
                throw new Exception("Failed to count pizzas.");
            }
            $count = $countQuery->fetchColumn();
            
            // Log count to error log
            error_log("Count of pizzas: " . $count);
    
            $offset = (int)($dayOfYear % $count);
    
            $stmt = $this->db->prepare("SELECT ID, name, description, price FROM Pizzas LIMIT 1 OFFSET ?");
            $stmt->bindValue(1, $offset, PDO::PARAM_INT);
            $stmt->execute();
    
            $pizza = $stmt->fetch(PDO::FETCH_ASSOC);
    
            if (!$pizza) {
                // If no pizza is returned by the offset, fallback to the first pizza
                $stmt = $this->db->query("SELECT ID, name, description, price FROM Pizzas ORDER BY id ASC LIMIT 1");
                $pizza = $stmt->fetch(PDO::FETCH_ASSOC);
                if (!$pizza) {
                    throw new Exception("No pizzas are available.");
                }
            }
    
            // Apply a 33% discount and round to 2 decimal places
            $pizza['price'] = round($pizza['price'] * 0.67, 2);
    
            return $pizza;
        } catch (Exception $e) {
            error_log($e->getMessage());
            // Debugging line: output the error message
            echo "Error: " . $e->getMessage();
    
            return ['error' => $e->getMessage()];
        }
    }
    
    
}
?>
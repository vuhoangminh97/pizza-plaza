// TODO: implement some nice JavaScript
function acceptCookies() {
    var expiryDate = new Date();
    expiryDate.setMonth(expiryDate.getMonth() + 6);
    localStorage.setItem('cookie-notice-accepted', expiryDate);
    document.getElementById('cookie-notice').style.display = 'none';
}

window.onload = function() {
    var acceptedDate = new Date(localStorage.getItem('cookie-notice-accepted'));
    if (acceptedDate <= new Date()) {
        document.getElementById('cookie-notice').style.display = 'block';
    }
};